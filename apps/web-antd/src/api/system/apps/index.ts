import type { AppsVO, AppsForm, AppsQuery } from './model';

import type { ID, IDS } from '#/api/common';
import type { PageResult } from '#/api/common';

import { commonExport } from '#/api/helper';
import { requestClient } from '#/api/request';

/**
 * 查询应用管理列表
 * @param params
 * @returns
 */
export function appsListInitialize(params?: AppsQuery) {
  return requestClient.get<AppsVO[]>('/system/apps/listInitialize', { params });
}

/**
 * 查询应用管理列表
 * @param params
 * @returns
 */
export function appsListAll(params?: AppsQuery) {
  return requestClient.get<AppsVO[]>('/system/apps/listAll',{  params });
}

/**
* 查询应用管理列表
* @param params
* @returns 应用管理列表
*/
export function appsList(params?: AppsQuery) {
  return requestClient.get<PageResult<AppsVO>>('/system/apps/list', { params });
}

/**
 * 导出应用管理列表
 * @param params
 * @returns 应用管理列表
 */
export function appsExport(params?: AppsQuery) {
  return commonExport('/system/apps/export', params ?? {});
}

/**
 * 查询应用管理详情
 * @param appsId id
 * @returns 应用管理详情
 */
export function appsInfo(appsId: ID) {
  return requestClient.get<AppsVO>(`/system/apps/${appsId}`);
}

/**
 * 新增应用管理
 * @param data
 * @returns void
 */
export function appsAdd(data: AppsForm) {
  return requestClient.postWithMsg<void>('/system/apps', data);
}

/**
 * 更新应用管理
 * @param data
 * @returns void
 */
export function appsUpdate(data: AppsForm) {
  return requestClient.putWithMsg<void>('/system/apps', data);
}

/**
 * 删除应用管理
 * @param appsId id
 * @returns void
 */
export function appsRemove(appsId: ID | IDS) {
  return requestClient.deleteWithMsg<void>(`/system/apps/${appsId}`);
}
