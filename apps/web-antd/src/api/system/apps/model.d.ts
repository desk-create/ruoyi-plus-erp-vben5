import type { PageQuery, BaseEntity } from '#/api/common';

export interface AppsVO {
  /**
   * 应用ID
   */
  appsId: string | number;

  /**
   * 图标【sys_oss.oss_id】
   */
  iconOssId: string | number;

  /**
   * 图标【sys_oss.oss_id】
   */
  iconOssIdUrl: string;

  /**
   * 应用名称
   */
  appsName: string;

  /**
   * 应用默认首页
   */
  baseHome: string;

  /**
   * 排序
   */
  sort: number;

  /**
   * 备注
   */
  remark: string;

}

export interface AppsForm extends BaseEntity {
  /**
   * 应用ID
   */
  appsId?: string | number;

  /**
   * 图标【sys_oss.oss_id】
   */
  iconOssId?: string | number;

  /**
   * 应用名称
   */
  appsName?: string;

  /**
   * 应用默认首页
   */
  baseHome?: string;

  /**
   * 排序
   */
  sort?: number;

  /**
   * 备注
   */
  remark?: string;

}

export interface AppsQuery extends PageQuery {
  /**
   * 图标【sys_oss.oss_id】
   */
  iconOssId?: string | number;

  /**
   * 应用名称
   */
  appsName?: string;

  /**
   * 应用默认首页
   */
  baseHome?: string;

  /**
   * 排序
   */
  sort?: number;

  /**
   * 日期范围参数
   */
  params?: any;
}
