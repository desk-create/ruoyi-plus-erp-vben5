import type { AxiosRequestConfig } from '@vben/request';

import type { FileCallBack } from '#/api/system/profile/model';

import { buildUUID } from '@vben/utils';

import { requestClient } from '#/api/request';

/**
 * Axios上传进度事件
 */
export type AxiosProgressEvent = AxiosRequestConfig['onUploadProgress'];

/**
 * 通过单文件上传接口
 * @param file 上传的文件
 * @param onUploadProgress 上传进度事件 非必传
 * @returns 上传结果
 */
export function uploadApi(
  file: Blob | File,
  onUploadProgress?: AxiosProgressEvent,
) {
  return requestClient.upload(
    '/resource/oss/upload',
    { file },
    { onUploadProgress, timeout: 60_000 },
  );
}


/**
 * 用户更新个人头像
 * @param fileCallback data
 * @returns void
 */
export function updateCropperPicture(fileCallback: FileCallBack) {
  /** 直接点击头像上传 filename为空 由于后台通过拓展名判断(默认文件名blob) 会上传失败 */
  let { file } = fileCallback;
  const { filename } = fileCallback;
  /**
   * Blob转File类型
   * 1. 在直接点击确认 filename为空 取uuid作为文件名
   * 2. 选择上传必须转为File类型 Blob类型上传后台获取文件名为空
   */
  file = filename
    ? new File([file], filename)
    : new File([file], `${buildUUID()}.png`);
  return requestClient.post(
    '/resource/oss/upload',
    {
      file,
    },
    { headers: { 'Content-Type': 'multipart/form-data' } },
  );
}

/**
 * 默认上传结果
 */
export interface UploadResult {
  url: string;
  fileName: string;
  ossId: string;
}
