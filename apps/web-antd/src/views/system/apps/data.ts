import type { FormSchemaGetter } from '#/adapter/form';
import type { VxeGridProps } from '#/adapter/vxe-table';


export const querySchema: FormSchemaGetter = () => [
  {
    component: 'Input',
    fieldName: 'appsName',
    label: '应用名称',
  },
];

export const columns: VxeGridProps['columns'] = [
  { type: 'checkbox', width: 60 },
  {
    title: 'ID',
    field: 'appsId',
    width: 60
  },
  {
    title: '图标',
    field: 'iconOssId',
    width: 60,
    slots: { default: 'iconOssId' },
  },
  {
    title: '应用名称',
    field: 'appsName',
  },
  {
    title: '应用默认首页',
    field: 'baseHome',
  },
  {
    title: '排序',
    field: 'sort',
  },
  {
    title: '备注',
    field: 'remark',
  },
  {
    field: 'action',
    fixed: 'right',
    slots: { default: 'action' },
    title: '操作',
    width: 180,
  },
];

export const modalSchema: FormSchemaGetter = () => [
  {
    label: '应用ID',
    fieldName: 'appsId',
    component: 'Input',
    dependencies: {
      show: () => false,
      triggerFields: [''],
    },
  },
  {
    label: '图标',
    fieldName: 'iconOssId',
    component: 'ImageUpload',
    componentProps: {
      // accept: ['jpg'], // 不支持type/*的写法 支持拓展名(不带.) 文件头(image/png这种)
      // maxNumber: 1, // 最大上传文件数 默认为1 为1会绑定为string而非string[]类型
      // resultField: 'url', // 上传成功后返回的字段名 默认url 可选['ossId', 'url', 'fileName']
    },
    rules: 'required',
  },
  {
    label: '应用名称',
    fieldName: 'appsName',
    component: 'Input',
    rules: 'required',
  },
  {
    label: '默认首页',
    fieldName: 'baseHome',
    component: 'Input',
    rules: 'required',
  },
  {
    label: '排序',
    fieldName: 'sort',
    component: 'InputNumber',
    rules: 'required',
    defaultValue: 0,
  },
  {
    label: '备注',
    fieldName: 'remark',
    component: 'Input',
  },
];
