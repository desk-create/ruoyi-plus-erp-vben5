export { default as CropperAvatar } from './src/cropper-avatar.vue';
export { default as CropperImage } from './src/cropper.vue';

export { default as CropperPicture } from './src/cropper-picture.vue';
export type { Cropper } from './src/typing';
